<#
.SYNOPSIS
    Author: Victor Smolinski
    08/18/2023
    Powershell script to push csv files to Atrius
.DESCRIPTION
    Pass folder path, Atrius credentials, and let the script run and listen on
    csv file create events. When a new file is created, push it to Atrius as soon
    as possible.
.EXAMPLE
    Set-ExecutionPolicy Bypass -Scope Process -Force;
    .\atrius_csv_push.ps1 
                          -gateway "my-gateway-id" \
                          -dir path\to\csv\directory \
                          -clientID client-id \
                          -secretID client-secret
#>
param(
    [Parameter(Mandatory = $true)] [string] $gateway,
    [Parameter(Mandatory = $true)] [string] $dir,
    [Parameter(Mandatory = $false)] [string] $clientID,
    [Parameter(Mandatory = $false)] [string] $secretID
)

# Setup
Add-Type -AssemblyName System.Net.Http
Clear-Host

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
[System.Net.ServicePointManager]::ServerCertificateValidationCallback = { $true }

# Functions
function Request-Token {
    param (
        [Parameter(Mandatory = $true)] [string] $clientID,
        [Parameter(Mandatory = $true)] [string] $secretID
    )
    
    try {
        $body = "client_id=$clientID&client_secret=$secretID&grant_type=client_credentials"
        $headers = New-Object "System.Collections.Generic.Dictionary[[String], [String]]"
        $headers.Add("Content-Type", "application/x-www-form-urlencoded")
        $response = Invoke-RestMethod 'https://api.buildingos.com/o/token/' -Method 'POST' -Headers $headers -Body $body
        return $response | Select-Object -ExpandProperty access_token
    }
    catch {
        Write-Host $_
    }
}

function Send-Csv {
    param (
        # [Parameter(Mandatory = $true)] [string] $token, ... Not used ??
        [Parameter(Mandatory = $true)] [string] $gateway,
        [Parameter(Mandatory = $true)] [string] $dir,
        [Parameter(Mandatory = $true)] [string] $csv
    )
    try {
        $multipartContent = [System.Net.Http.MultipartFormDataContent]::new()
        $multipartFile = $dir + "\" + $csv
        $FileStream = [System.IO.FileStream]::new($multipartFile, [System.IO.FileMode]::Open)
        $fileHeader = [System.Net.Http.Headers.ContentDispositionHeaderValue]::new("form-data")
        $fileHeader.Name = "file"
        $fileHeader.FileName = $csv
        $fileContent = [System.Net.Http.StreamContent]::new($FileStream)
        $fileContent.Headers.ContentDisposition = $fileHeader
        $multipartContent.Add($fileContent)
        
        $body = $multipartContent
        $connectionString = "https://rest.buildingos.com/dsv/push/?datasource=" + $gateway
        
        $response = Invoke-RestMethod $connectionString -Method 'POST' -Headers $headers -Body $body
        $response | ConvertTo-Json
    }
    catch {
        Write-Host $_
    }
}

function Watch-Folder {
    param(
        [Parameter(Mandatory = $true)] [string] $dir
    )    
    $timeout = 1000
    $log = "log.txt"

    try {
        $FileSystemWatcher = New-Object System.IO.FileSystemWatcher $dir
        $FileSystemWatcher.IncludeSubdirectories = $true
        $FileSystemWatcher.Filter = "*.csv"
        $FileSystemWatcher.EnableRaisingEvents = $true

        Write-Host "Monitoring content of $dir"
        while ($true) {
            $change = $FileSystemWatcher.WaitForChanged('Created', $timeout)
            if ($change.TimedOut -eq $false) {
                $change | Out-Default
                    (Get-Date).ToString() + ", " + $change.ChangeType.ToString() + ", " + $change.Name | 
                Out-File $log -Append

                Send-Csv $gateway $dir $change.Name
            }
            else {
                Write-Host "*" -NoNewline
            }
        }
    }
    finally {
        $FileSystemWatcher.Dispose()
        Write-Host "Folder Watcher is done."
    }
}

Watch-Folder $dir